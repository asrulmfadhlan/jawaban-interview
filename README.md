```
CREATE TABLE `akun` (
  `id` int PRIMARY KEY,
  `username` varchar(255),
  `hp` int,
  `email` varchar(255),
  `password` varchar(255)
);

CREATE TABLE `driver` (
  `id` int PRIMARY KEY,
  `username` varchar(255),
  `hp` int,
  `email` varchar(255),
  `ratting` int,
  `jumtrip` int
);

CREATE TABLE `transaksi` (
  `id` int PRIMARY KEY,
  `runtime_driver` varchar(255),
  `waktu` int,
  `komentar` varchar(255),
  `chat` text,
  `id_keranjang` varchar(255),
  `id_pelanggan` int,
  `id_toko` int,
  `id_kategori` int
);

CREATE TABLE `pembayaran` (
  `id` int PRIMARY KEY,
  `biaya` int,
  `saldo` int,
  `jenis` varchar(255),
  `ongkir` int,
  `biayaadm` int,
  `id_promo` int
);

CREATE TABLE `pelanggan` (
  `id` int PRIMARY KEY,
  `nama` varchar(255),
  `alamat` varchar(255),
  `hp` int,
  `laporan` text
);

CREATE TABLE `toko` (
  `id` int PRIMARY KEY,
  `nama` varchar(255),
  `alamat` varchar(255),
  `ratting` int,
  `menu` varchar(255)
);

CREATE TABLE `promo` (
  `id` int PRIMARY KEY,
  `jenispromo` varchar(255),
  `codepromo` varchar(255),
  `masaberlaku` varchar(255)
);

CREATE TABLE `keranjang` (
  `id` int PRIMARY KEY,
  `pesanan1` varchar(255),
  `harga_psn1` int,
  `pesanan2` varchar(255),
  `harga_psn2` int,
  `totalharga` int
);

CREATE TABLE `kendaraan` (
  `id_kendaraan` int,
  `merk_kendaraan` varchar(255),
  `plat_no` varchar(255),
  `warna` varchar(255)
);

CREATE TABLE `Kategori` (
  `id` int,
  `Makanan_brt` varchar(255),
  `cemilan` varchar(255),
  `minuman` varchar(255)
);

INSERT INTO akun (id, username, hp, email, password)
VALUES (1, 'budiman', 089963726, 'Budiman321', 'zanisa213'),
       (2, 'Abdul', 083642934, 'abdul78@gmail.com', 'gegep321'),
       (3, 'Rangga', 081237958, 'ranggakun66@gmail.com', 'gofood456');


INSERT INTO driver (id, username, hp, email, ratting, jumtrip)
VALUES (1, 'ican', 08996352739, 'icanqod76@gmail.com', 4, 50),
       (2, 'ahsan', 08215364845, 'ahsan76@gmail.com', 3, 30),
       (3, 'rama', 08274658564, 'raminal@gmail.com', 5, 100);
       (4, 'daffa', 08274234234, 'daffa55l@gmail.com', 4, 120);

INSERT INTO transaksi (id, runtime_driver, waktu, komentar, chat, id_keranjang, id_pelanggan, id_toko, id_kategori)
VALUES (1, 'driver1', 1600, 'Pengiriman cepat dan aman.', 'Terima kasih!', 'KR123', 1, 1, 1),
       (2, 'driver2', 1700, 'Makanan enak, pelayanan bagus.', 'Silakan pesan lagi!', 'KR456', 2, 2, 2),
       (3, 'driver3', 1800, 'Pesanan lengkap, terima kasih!', 'Terima kasih!', 'KR789', 3, 3, 3);

INSERT INTO pembayaran (id, biaya, saldo, jenis, ongkir, biayaadm, id_promo)
VALUES (1, 50000, 100000, 'GoPay', 5000, 1000, 1),
       (2, 30000, 200000, 'OVO', 3000, 500, 2),
       (3, 25000, 150000, 'Dana', 2000, 800, 3);

INSERT INTO pelanggan (id, nama, alamat, hp, laporan)
VALUES (1, 'zulfi', 'Jalan Padalarang 66', 08213449789, 'Laporan pesanan tidak sesuai'),
       (2, 'bacil', 'Jalan cileunyi 9', 089987654321, 'Laporan pesanan terlambat'),
       (3, 'ayuy', 'Jalan gatsu No. 5', 0856385555, 'Laporan pesanan hilang');

INSERT INTO toko (id, nama, alamat, ratting, menu)
VALUES (1, 'Toko A', 'Jalan Rancabali No. 123', 4, 'Nasi goreng, Nasi padang, rendang'),
       (2, 'Toko B', 'Jalan cikeas No. 9', 3, 'corndog, cireng, seblak'),
       (3, 'Toko C', 'Jalan sasak saat No. 5', 5, 'milk tea, boba milk, teh tarik');

INSERT INTO promo (id, jenispromo, codepromo, masaberlaku)
VALUES (1, 'Diskon', 'PROMO50', '2023-06-30'),
       (2, 'Gratis Ongkir', 'FREEDELIVERY', '2023-05-31'),
       (3, 'Cashback', 'CASHBACK10', '2023-07-15');

INSERT INTO keranjang (id, pesanan1, harga_psn1, pesanan2, harga_psn2, totalharga)
VALUES (1, 'Makanan A', 25000, 'Minuman X', 10000, 35000),
       (2, 'Makanan B', 30000, 'Minuman Y', 12000, 42000),
       (3, 'Makanan C', 35000, 'Minuman Z', 15000, 50000);

INSERT INTO kendaraan (id_kendaraan, merk_kendaraan, plat_no, warna)
VALUES (1, 'Toyota Avanza', 'B 1234 AB', 'Silver'),
       (2, 'Honda Scoopy', 'H 5678 CD', 'Black'),
       (3, 'Suzuki Ertiga', 'D 9012 EF', 'White');

INSERT INTO Kategori (id, Makanan_brt, cemilan, minuman)
VALUES (1, 'Makanan Berat', 'Snack', 'Minuman'),
       (2, 'Makanan Ringan', 'Snack', 'Minuman'),
       (3, 'Makanan Berat', 'Snack', 'Minuman');



ALTER TABLE `transaksi` ADD FOREIGN KEY (`id`) REFERENCES `driver` (`id`);

ALTER TABLE `pembayaran` ADD FOREIGN KEY (`id`) REFERENCES `transaksi` (`id`);

ALTER TABLE `transaksi` ADD FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id`);

ALTER TABLE `pembayaran` ADD FOREIGN KEY (`id_promo`) REFERENCES `promo` (`id`);

ALTER TABLE `transaksi` ADD FOREIGN KEY (`id_keranjang`) REFERENCES `keranjang` (`id`);

ALTER TABLE `transaksi` ADD FOREIGN KEY (`id_toko`) REFERENCES `toko` (`id`);

ALTER TABLE `toko` ADD FOREIGN KEY (`id`) REFERENCES `akun` (`id`);

ALTER TABLE `pelanggan` ADD FOREIGN KEY (`id`) REFERENCES `akun` (`id`);

ALTER TABLE `akun` ADD FOREIGN KEY (`id`) REFERENCES `driver` (`id`);

ALTER TABLE `kendaraan` ADD FOREIGN KEY (`id_kendaraan`) REFERENCES `driver` (`id`);

ALTER TABLE `Kategori` ADD FOREIGN KEY (`id`) REFERENCES `transaksi` (`id_kategori`);

UPDATE pembayaran
SET payment_time = '2023-04-12 10:10:00'
WHERE id = 1;

SELECT MONTH(payment_time) AS bulan, COUNT(*) AS jumlah_transaksi
FROM pembayaran
GROUP BY MONTH(payment_time);

SELECT * FROM driver;

SELECT strftime('%m', payment_time) AS month, COUNT(*) AS transaction_count
FROM pembayaran
GROUP BY month;
```
