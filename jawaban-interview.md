### 1. Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)

```java

public class OneDriveApp {
    // ...

    private static void displayFileSystem() {
        System.out.println("Struktur File System:");
        int totalSize = displayFolderStructure(rootFolder, 0);
        System.out.println("Total Size: " + totalSize + " bytes");
    }

    private static int displayFolderStructure(Folder folder, int indentLevel) {
        String indent = "";
        for (int i = 0; i < indentLevel; i++) {
            indent += "\t";
        }

        System.out.println(indent + "- " + folder.getName());
        int totalSize = 0;
        for (FileSystemItem item : folder.getItems()) {
            if (item instanceof Folder) {
                Folder subFolder = (Folder) item;
                int subFolderSize = displayFolderStructure(subFolder, indentLevel + 1);
                totalSize += subFolderSize;
            } else if (item instanceof File) {
                File file = (File) item;
                System.out.println(indent + "\t- " + file.getName() + " (Size: " + file.getSize() + " bytes)");
                totalSize += file.getSize();
            }
        }
        return totalSize;
    }
}
```
Saat melakukan rekursi ke dalam folder yang lebih dalam, disana diakumulasi ukuran file dengan menambahkan ukuran setiap file ke totalSize. Akhirnya, kami mengembalikan totalSize dari metode displayFolderStructure(), sehingga kita dapat mencetak total ukuran file setelah menampilkan struktur file system.


### 2. Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)
1. User Melakukan Registrasi dan Login

```java
switch (choice) {
                case 1:
                    registerUser(scanner);
                    break;
                case 2:
                    login(scanner);
                    break;
                case 3:
                    if (currentUser != null) {
                        runOneDrive(scanner);
                    } else {
                        System.out.println("Anda harus login terlebih dahulu.");
                    }
                    break;
                case 4:
                    System.out.println("Terima kasih telah menggunakan OneDrive!");
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan coba lagi.");
                    break;
            }
```
Jadi jika memasukan username yang seusai kita dapat masuk ke dalam program OneDrive, tetapi sebelum itu user harus registrasi terlebih daahulu 
```java
private static void registerUser(Scanner scanner) {
        System.out.print("Masukkan username: ");
        String username = scanner.nextLine();
        System.out.print("Masukkan email: ");
        String email = scanner.nextLine();
        System.out.print("Masukkan password: ");
        String password = scanner.nextLine();

        if (userRegistration.isUsernameTaken(username)) {
            System.out.println("Username \"" + username + "\" sudah digunakan. Silakan pilih username lain.");
        } else if (userRegistration.isEmailTaken(email)) {
            System.out.println("Email \"" + email + "\" sudah digunakan. Silakan pilih email lain.");
        } else {
            userRegistration.registerUser(username, email, password);
        }
    }
```
Kemudian akan di cek di kelas "login" apakah username daan password sesuaai atau tidak 

```java
private static void login(Scanner scanner) {
        System.out.print("Masukkan username: ");
        String username = scanner.nextLine();
        if (userRegistration.isUsernameTaken(username)) {
            currentUser = new User(username, "", "");
            rootFolder = new Folder(username);
            System.out.println("Login berhasil. Selamat datang, " + username + "!");
        } else {
            System.out.println("Username \"" + username + "\" tidak ditemukan. Silakan registrasi terlebih dahulu.");
        }
    }
```
2. Mencari file 
```java
private static void searchFile(Scanner scanner) {
        System.out.print("Masukkan nama file yang ingin dicari: ");
        String fileName = scanner.nextLine();
        System.out.println("Hasil Pencarian:");
        searchFile(rootFolder, fileName);
    }

    private static void searchFile(Folder folder, String fileName) {
        for (FileSystemItem item : folder.getItems()) {
            if (item instanceof Folder) {
                searchFile((Folder) item, fileName);
            } else if (item instanceof File) {
                File file = (File) item;
                if (file.getName().equalsIgnoreCase(fileName)) {
                    System.out.println("- " + file.getName() + "." + file.getExtension() + " (File) (Size: " + file.getSize() + " bytes)");
                }
            }
        }
    }
```
saya menggunakan if else untuk mensinkron kan antarasetiap kata yang di cari dan juga menggunakan for untuk mengulang pencarian setiap hurup hingga sesuai dengan kata (kata dari nama file) yang di cari 

3. Menampilkan struktur setiap file yang ada dari setiap folder

```java
private static void displayFileSystem() {
        System.out.println("==================");
        System.out.println("Struktur File System");
        System.out.println("==================");
        displayFolder(rootFolder, 0);
    }

    private static void displayFolder(Folder folder, int level) {
        String indentation = getIndentation(level);
        System.out.println(indentation + "- " + folder.getName() + " (Folder) (Size: " + folder.getSize() + " bytes)");
        for (FileSystemItem item : folder.getItems()) {
            if (item instanceof Folder) {
                displayFolder((Folder) item, level + 1);
            } else if (item instanceof File) {
                File file = (File) item;
                System.out.println(getIndentation(level + 1) + "- " + file.getName() + "." + file.getExtension() + " (File) (Size: " + file.getSize() + " bytes)");
            }
        }
    }
```
Disini saya dapat menampilakan struktur dari setiap folder beserta dengan hierarki dari setiap folder terdapat filse apasaja di dalam nya beserta dengan ukuran dari setiap file dan folder, untuk folder yaitu menjumlahkan ukuran dari setiap folder dan file yang ada dalam hierarkitersebut


### 3. Mampu menjelaskan konsep dasar OOP
Konsep OOP (Object-Oriented Programming) adalah pendekatan dalam pemrograman yang mengorganisasi dan memodelkan perangkat lunak berdasarkan objek-objek yang memiliki karakteristik dan perilaku tertentu. Berikut adalah penjelasan sederhana tentang konsep OOP:

1. Objek: Objek adalah instansi dari sebuah kelas. Objek memiliki atribut (data) yang mewakili keadaan objek dan metode (fungsi) yang mewakili perilaku objek. Contoh objek dalam kehidupan sehari-hari adalah mobil, buku, atau manusia.

2. Kelas: Kelas adalah blueprint atau template yang mendefinisikan struktur dan perilaku objek. 

3. Enkapsulasi: Enkapsulasi adalah konsep yang menggabungkan data dan metode terkait dalam sebuah objek.enkapsulasi membantu dalam mencapai konsep "pengkapsulan" dan "informasi tersembunyi".

4. Pewarisan (Inheritance): Pewarisan memungkinkan pembuatan kelas baru (kelas anak) yang mewarisi atribut dan metode dari kelas yang ada (kelas induk). Dengan pewarisan, kelas anak dapat memperluas atau mengubah perilaku yang diwarisi dari kelas induk. 

5. Polymorphism: Polimorfisme memungkinkan penggunaan metode dengan nama yang sama namun dengan perilaku yang berbeda pada objek-objek yang berbeda. 

6. Abstraksi: Abstraksi melibatkan penyingkatan dan penyederhanaan kompleksitas objek dengan hanya memperhatikan aspek yang penting dan mengabaikan detail yang tidak relevan. 

Konsep OOP ini membantu dalam mengorganisasi dan mempermudah pengembangan perangkat lunak dengan memisahkan logika menjadi objek-objek terpisah yang saling berinteraksi. Dengan OOP, kode menjadi lebih terstruktur, dapat digunakan kembali, dan lebih mudah dipelihara.

### 4. Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

Encapsulation adalah salah satu pilar OOP yang memiliki fungsi untuk memiliki fungsi melidungi suatu variable atau method agar tidak dapat di akses dari class yang tidak di tentukan

1. Public & Private
```java
class Folder extends FileSystemItem {
    private List<FileSystemItem> items;

    public Folder(String name) {
        super(name);
        items = new ArrayList<>();
    }

    public void addItem(FileSystemItem item) {
        items.add(item);
        updateFolderSize();
    }

    public void removeItem(FileSystemItem item) {
        items.remove(item);
        updateFolderSize();
    }

    public List<FileSystemItem> getItems() {
        return items;
    }

    private void updateFolderSize() {
        int totalSize = 0;
        for (FileSystemItem item : items) {
            totalSize += item.getSize();
        }
        setSize(totalSize);
    } 
```
Disini saya menampilkan class folder{} yang memiliki fungsi untuk membuat folder baru saya membuat folder baru. Dalam class folder() terdapat Encapsulation yaitu public dan private, public di gunakan pada method folder(), addItem(), removeItem(). Dan private ada pada method updateFolderSize() yang berfungsi untuk menjumlahkan size pada suatu folder saat folder tersebut mempunyai item

2. Protected
```java
abstract class FileSystemItem {
    protected String name;
    protected int size;

    public FileSystemItem(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public abstract void display();

    protected void setSize(int size) {
        this.size = size;
    }
}
```
Dalam code di atas memiliki Encapsulation Protected yang terrdapat pada method setSize() yang berfungsi untuk menampilkan ukuran folder dan file yang dimana class folder merupakan class turunan dari kelas FileSystemItem{}

### 5. Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)

Abstraction merupakan Pilar OOP yang berperan sebagai blueprint dari program yang memiliki fungsi untuk mendefinisikan metode tanpa memberikan implementasi yang spesifik

```java
abstract class FileSystemItem {
    protected String name;
    protected int size;

    public FileSystemItem(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public abstract void display();

    protected void setSize(int size) {
        this.size = size;
    }
}

class Folder extends FileSystemItem {
    private List<FileSystemItem> items;

    public Folder(String name) {
        super(name);
        items = new ArrayList<>();
    }

    public void addItem(FileSystemItem item) {
        items.add(item);
        updateFolderSize();
    }

    public void removeItem(FileSystemItem item) {
        items.remove(item);
        updateFolderSize();
    }

    public List<FileSystemItem> getItems() {
        return items;
    }

    private void updateFolderSize() {
        int totalSize = 0;
        for (FileSystemItem item : items) {
            totalSize += item.getSize();
        }
        setSize(totalSize);
    }

    @Override
    public void display() {
        System.out.println("Folder: " + getName() + " (Size: " + getSize() + " bytes)");
        for (FileSystemItem item : items) {
            item.display();
        }
    }
}

class File extends FileSystemItem {
    private String extension;
    private String uploader;

    public File(String name, String extension, int size) {
        super(name);
        this.extension = extension;
        setSize(size);
    }

    public String getExtension() {
        return extension;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    @Override
    public void display() {
        System.out.println("File: " + getName() + " (Size: " + getSize() + " bytes)");
    }
}
```

Pada contoh di atas, objek file dan folder dideklarasikan dengan tipe FileSystemItem, yang merupakan kelas abstrak. Meskipun demikian, kita masih dapat membuat objek dengan tipe tersebut menggunakan polimorfisme.

Ketika memanggil metode display() pada objek file dan folder, pemanggilan metode tersebut akan menjalankan implementasi yang ada di kelas turunan, yaitu File dan Folder. Dengan menggunakan abstraksi, kita dapat memisahkan antara konsep umum yang terdapat pada kelas abstrak dan implementasi spesifik yang ada pada kelas turunannya

### 6. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

1. Inheritance merupakan Pewarisan dari parent class ke childe class yang melibatkan pembuatan class baru dengan mewarisi atribut dan metode dri superclass nya

```java
abstract class FileSystemItem {
    protected String name;
    protected int size;

    public FileSystemItem(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public abstract void display();

    protected void setSize(int size) {
        this.size = size;
    }
}

class Folder extends FileSystemItem {
    private List<FileSystemItem> items;

    public Folder(String name) {
        super(name);
        items = new ArrayList<>();
    }

    public void addItem(FileSystemItem item) {
        items.add(item);
        updateFolderSize();
    }

    public void removeItem(FileSystemItem item) {
        items.remove(item);
        updateFolderSize();
    }

    public List<FileSystemItem> getItems() {
        return items;
    }

    private void updateFolderSize() {
        int totalSize = 0;
        for (FileSystemItem item : items) {
            totalSize += item.getSize();
        }
        setSize(totalSize);
    }

    @Override
    public void display() {
        System.out.println("Folder: " + getName() + " (Size: " + getSize() + " bytes)");
        for (FileSystemItem item : items) {
            item.display();
        }
    }
}

class File extends FileSystemItem {
    private String extension;
    private String uploader;

    public File(String name, String extension, int size) {
        super(name);
        this.extension = extension;
        setSize(size);
    }

    public String getExtension() {
        return extension;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    @Override
    public void display() {
        System.out.println("File: " + getName() + " (Size: " + getSize() + " bytes)");
    }
}

class User {
    private String username;
    private String email;
    private String password;

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
```
class File dan Folder merupakan turunan dari class FileSystemItem. Dengan menggunakan inheritance, class File dan Folder mewarisi atribut dan metode dari class FileSystemItem, seperti name dan display().

2. Polymorphism  tidak berbeda jauh dengan Inheritace yaitu penurunan tetapi dapat memungkinkan suatu objek untuk diperlakukan sebagai objek dari tipe yang berbeda dalam hierarki pewarisan, namun masih mempertahankan perilaku yang sesuai dengan tipe aslinya. 

```java
class File extends FileSystemItem {
    private String content;

    public File(String name, String content) {
        super(name);
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    @Override
    public void display() {
        System.out.println("File: " + getName());
    }
}

// Subclass untuk folder
class Folder extends FileSystemItem {
    private List<FileSystemItem> items;

    public Folder(String name) {
        super(name);
        items = new ArrayList<>();
    }

    public void addItem(FileSystemItem item) {
        items.add(item);
    }

    public List<FileSystemItem> getItems() {
        return items;
    }

    @Override
    public void display() {
        System.out.println("Folder: " + getName());
        for (FileSystemItem item : items) {
            item.display();
        }
    }
}
```
Metode display() dideklarasikan di kelas FileSystemItem sebagai metode abstrak.<br>
Metode display() diimplementasikan secara berbeda di kelas File dan Folder.<br>
Ketika memanggil metode display() pada objek File atau Folder, metode yang sesuai akan dipanggil tergantung pada jenis objek yang digunakan.

### 7. Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

1. User melakukan registrasi : <br>
<p1>ketika user ingin menggunakan OneDrive maka langkah utama adalah melakukan registrasi karena jika tidak melakukan registrasi tidak akan dapat mengaakses OneDrive.<br>
Disini saya membuaat class Register yang mempunyai fungsi untuk membuat sekaligus menmpung akun yang telah di buat.<p1>
```java
private static void registerUser(Scanner scanner) {
        System.out.print("Masukkan username: ");
        String username = scanner.nextLine();
        System.out.print("Masukkan email: ");
        String email = scanner.nextLine();
        System.out.print("Masukkan password: ");
        String password = scanner.nextLine();

        if (userRegistration.isUsernameTaken(username)) {
            System.out.println("Username \"" + username + "\" sudah digunakan. Silakan pilih username lain.");
        } else if (userRegistration.isEmailTaken(email)) {
            System.out.println("Email \"" + email + "\" sudah digunakan. Silakan pilih email lain.");
        } else {
            userRegistration.registerUser(username, email, password);
        }
}
```


2. User dapat login <br>
Jika sudah registrasi user dapat login menggunakan username dan password yang telah di registrasi.<br>
Kemudian saya membuat class login untuk memverifikasi akun akun yang telh di buat.
```java
private static void login(Scanner scanner) {
        System.out.print("Masukkan username: ");
        String username = scanner.nextLine();
        if (userRegistration.isUsernameTaken(username)) {
            currentUser = new User(username, "", "");
            rootFolder = new Folder(username);
            System.out.println("Login berhasil. Selamat datang, " + username + "!");
        } else {
            System.out.println("Username \"" + username + "\" tidak ditemukan. Silakan registrasi terlebih dahulu.");
        }
}
```

3. User dapat Mengunggah file dan folder <br>
Di dalam OneDrive user dapat membuat folder, dan juga Mengunggah file dengan memasukan tipe file dan ukuran file. <br> 
Saya membuat class creatfolder() dan uploadfile() yaang memliki fungsi untuk mengunggah file dan membuat folder.
```java
class Folder extends FileSystemItem {
    private List<FileSystemItem> items;

    public Folder(String name) {
        super(name);
        items = new ArrayList<>();
    }

    public void addItem(FileSystemItem item) {
        items.add(item);
        updateFolderSize();
    }

    public void removeItem(FileSystemItem item) {
        items.remove(item);
        updateFolderSize();
    }

    public List<FileSystemItem> getItems() {
        return items;
    }

    private void updateFolderSize() {
        int totalSize = 0;
        for (FileSystemItem item : items) {
            totalSize += item.getSize();
        }
        setSize(totalSize);
    }

    @Override
    public void display() {
        System.out.println("Folder: " + getName() + " (Size: " + getSize() + " bytes)");
        for (FileSystemItem item : items) {
            item.display();
        }
    }
}

class File extends FileSystemItem {
    private String extension;
    private String uploader;

    public File(String name, String extension, int size) {
        super(name);
        this.extension = extension;
        setSize(size);
    }

    public String getExtension() {
        return extension;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    @Override
    public void display() {
        System.out.println("File: " + getName() + " (Size: " + getSize() + " bytes)");
    }
}
```


4. User dapat mencari file <br>
Setelah membuat folder dan Mengunggah file user juga dapat mencari file yang telah di unggah. <br>
Membuat class searchFile() adalah cara saya agar dapaat mencri file.
```java
private static void searchFile(Folder folder, String fileName) {
        for (FileSystemItem item : folder.getItems()) {
            if (item instanceof Folder) {
                searchFile((Folder) item, fileName);
            } else if (item instanceof File) {
                File file = (File) item;
                if (file.getName().equalsIgnoreCase(fileName)) {
                    System.out.println("- " + file.getName() + "." + file.getExtension() + " (File) (Size: " + file.getSize() + " bytes)");
                }
            }
        }
    }
```


5. User dapat menampilakn struktur file dan folder <br>
Karena kita belum bisa menentukan lokasi file yang di unggah jadi saya membuat class displayFileSystem() yang dimana kita dapat menampilkan struktur file dan folder.
```java
private static void displayFolder(Folder folder, int level) {
        String indentation = getIndentation(level);
        System.out.println(indentation + "- " + folder.getName() + " (Folder) (Size: " + folder.getSize() + " bytes)");
        for (FileSystemItem item : folder.getItems()) {
            if (item instanceof Folder) {
                displayFolder((Folder) item, level + 1);
            } else if (item instanceof File) {
                File file = (File) item;
                System.out.println(getIndentation(level + 1) + "- " + file.getName() + "." + file.getExtension() + " (File) (Size: " + file.getSize() + " bytes)");
            }
        }
    }
```


6. User dapat mengunggah file di lokasi yang kita tentukan
Jika kita tidak dapat menentukan lokasi file yang ingin di unggah maka file tersebut akan menjadi berantakan dan susah di cari secara manual.
```java
File newFile = new File(fileName, fileExtension, fileSize);
        newFile.setUploader(username);
        if (folderLocation.equals("/")) {
            rootFolder.addItem(newFile);
            System.out.println("File \"" + fileName + "." + fileExtension + "\" berhasil diunggah ke folder " + username);
        } else {
            Folder targetFolder = findFolder(folderLocation);
            if (targetFolder != null) {
                targetFolder.addItem(newFile);
                System.out.println("File \"" + fileName + "." + fileExtension + "\" berhasil diunggah ke folder \"" + folderLocation + "\".");
            } else {
                System.out.println("Folder \"" + folderLocation + "\" tidak ditemukan.");
            }
        }
```


### 8. Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)
| No. | Use Case dan Penjelasan                                                                                         | Nilai |
|-----|---------------------------------------------------------------------------------------------------|-------|
| 1.  | Mengunggah file dari perangkat lokal ke OneDrive: <br>saya menggunakan class FileSystemItem{}, file{} Merupakan turunan dari class FileSystemItem{}, kemudian di proses dengan method uploadfile(), dengan menggunakan method getExtension() untuk memasukan type jenis filenya, display() untuk menampilkan nama,dan ukuran                                                | 10     |
| 2.  | Melkukan registerUser: <br> terdapat class userRegistration() untuk menambahkan data user untuk login, kemudian di proses kembali di class login untuk memverivikasi username dan password jika berhasil maka user dapat masuk kedalam aplikasi OneDrive. method yang di gunakan registerUser() yang berguna untuk meregistrasi, method login() yang dimana method ini berfungsi untuk memverivikasi usernya apakah sudah terdaftar atau belum                                                                             | 10     |
| 3.  | Membuat Folder: <br> ada class FileSystemItem{} yang menurunkan class folder{} yang dimana terdapat method addItem(), removeItem(), getSize()                                                                                 | 10     |
| 4.  | Menampilkan folder dan file di OneDrive: <br> menggunakan method displayFileSystem(), untuk menampilakan struktur file yang terhubung dengan method displayFolder()                                            | 9     |
| 5.  | Menghapus file yang tidak diperlukan dari OneDrive.                                                               | 8     |
| 6.  | Mengunduh file dari OneDrive ke perangkat lokal.                                                  | 9     |
| 7.  | Mengedit file langsung di OneDrive.                                               | 8     |
| 8.  | Membuat folder baru di OneDrive.                                                                  | 7     |
| 9.  | Mengganti nama file dan folder di OneDrive.                                                       | 7     |
| 10. | Mencari file dan folder di OneDrive.                                                              | 8     |
| 11. | Membuat salinan file di OneDrive.                                                                 | 8     |
| 12. | Membuka dan melihat file di OneDrive.                                                             | 8     |
| 13. | Mengatur hak akses dan izin berbagi untuk file dan folder di OneDrive.                            | 9     |
| 14. | Mengatur file dan folder dengan kategori dan tag di OneDrive.                                     | 7     |
| 15. | Mengelola versi file di OneDrive.                                                                 | 8     |
| 16. | Membatalkan tindakan atau pemulihan file yang dihapus di OneDrive.                                | 8     |
| 17. | Mengatur pemberitahuan dan notifikasi untuk aktivitas di OneDrive.                                | 7     |
| 18. | Mengarsipkan file dan folder di OneDrive.                                                         | 7     |
| 19. | Mengatur pemulihan file yang hilang atau terhapus di OneDrive.                                    | 8     |
| 20. | Mengedit metadata file dan folder di OneDrive.                                                    | 7     |
| 21. | Menandai file dan folder favorit di OneDrive untuk akses cepat.                                   | 6     |
| 22. | Mengatur dan mengelola album foto di OneDrive.                                                    | 8     |
| 23. | Mengelola file audio dan video di OneDrive.                                                       | 7     |
| 24. | Mengelola dan mengedit dokumen di OneDrive.                                                       | 9     |
| 25. | Mengelola file PDF di OneDrive.                                                                   | 8     |
| 26. | Mengorganisir file dan folder dengan fitur tata letak di OneDrive.                                | 7     |
| 27. | Mengelola file yang dibagikan oleh pengguna lain di OneDrive.                                     | 8     |
| 28. | Mengamankan file dengan enkripsi di OneDrive.                                                     | 9     |
| 29. | Menggunakan OneDrive sebagai cadangan dan penyimpanan file.                                       | 9     |
| 30. | Mengelola perangkat yang terhubung dengan OneDrive.                                              | 7     |
| 31. | Mengatur jadwal sinkronisasi OneDrive dengan perangkat lokal.                                     | 8     |
| 32. | Mengelola batasan ruang penyimpanan di OneDrive.                                                  | 7     |
| 33. | Menggabungkan dan membagikan folder di OneDrive.                                                  | 8     |
| 34. | Mengimpor file dari layanan cloud lain ke OneDrive.                                               | 8     |
| 35. | Mengintegrasikan OneDrive dengan aplikasi pihak ketiga.                                           | 8     |
| 36. | Melihat riwayat aktivitas dan log di OneDrive.                                                    | 7     |
| 37. | Mengelola akses jarak jauh ke OneDrive.                                                           | 8     |
| 38. | Menggunakan OneDrive di perangkat mobile.                                                         | 9     |
| 39. | Mengatur tautan berbagi yang memiliki tanggal kedaluwarsa di OneDrive.                            | 7     |
| 40. | Mengatur folder dan file dengan pemindahan batch di OneDrive.                                     | 8     |

![Dokumentsi](Dokumentasi/ClassDiagrm.png)

### 9. Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

### 10. Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)
1. Membuat Folder <br>
![Dokumentasi](Dokumentasi/BuatFolder.png)

2. Menu Mengunngah File <br>
![Dokumentasi](Dokumentasi/UnggahFile.png)

3. Menu Tampilkan File <br>
![Dokumentasi](Dokumentasi/TampilkanFIle.png)

4. Menu Cari File <br>
![Dokumentasi](Dokumentasi/CariFile.png)

5. Menu Hapus Folder <br>
![Dokumentasi](Dokumentasi/HapusFolder.png)

6. Login <br>
![Dokumentasi](Dokumentasi/Login.png)

